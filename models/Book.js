var mongoose = require('mongoose');

var BookSchema = new mongoose.Schema({
  nama_pelajar: String,
  title: String,
  no_matrik: String,
  faculty:String,
  school:String,
  Gender: String,
  nationality: String,
  races: String,
  address: String,
  email:String,
  phone: String,
  
  updated_tarikh: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Book', BookSchema);
