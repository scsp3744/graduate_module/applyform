import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.css']
})
export class BookEditComponent implements OnInit {

  applyForm: FormGroup;
  id:string = '';
  nama_pelajar:string = '';
  title:string = '';
  Gender:string = '';
  no_matrik:string = '';
  faculty:string = '';
  school:string = '';
  nationality:string = '';
  races:string = '';
  address:string='';
  email:string='';
  phone:string='';
  
  matcher = new MyErrorStateMatcher();
  
  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getBook(this.route.snapshot.params['id']);
    this.applyForm = this.formBuilder.group({
      'nama_pelajar' : [null, Validators.required],
      'title' : [null, Validators.required],
      'Gender' : [null, Validators.required],
      'no_matrik' : [null, Validators.required],
      'faculty' : [null, Validators.required],
      'school' : [null, Validators.required],
      'nationality' : [null, Validators.required],
      'races' : [null, Validators.required],
      'address' : [null, Validators.required],
      'email' : [null, Validators.required],
      'phone' : [null, Validators.required]
    });
  }

  getBook(id) {
    this.api.getBook(id).subscribe(data => {
      this.id = data._id;
      this.applyForm.setValue({
        nama_pelajar: data.nama_pelajar,
        title: data.title,
        Gender: data.Gender,
        faculty: data.faculty,
        school: data.school,
        no_matrik: data.no_matrik,
        nationality: data.nationality,
        races: data.races,
        address: data.address,
        email: data.email,
        phone: data.phone

      });
    });
  }

  onFormSubmit(form:NgForm) {
    this.api.updateBook(this.id, form)
      .subscribe(res => {
          let id = res['_id'];
          this.router.navigate(['/book-details', id]);
        }, (err) => {
          console.log(err);
        }
      );
  }

  bookDetails() {
    this.router.navigate(['/book-details', this.id]);
  }
}
