import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}



@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html',
  styleUrls: ['./book-create.component.css']



})
export class BookCreateComponent implements OnInit {

  applyForm: FormGroup;
  nama_pelajar:string='';
  title:string='';
  genderControl = new FormControl('', Validators.required);
  Gender : string='';
  no_matrik:string='';
  faculty:string='';
  school:string='';
  nationality:string='';
  races:string='';
  address:string='';
  email:string='';
  phone:string='';
  
  matcher = new MyErrorStateMatcher();

  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit() :void {
    this.applyForm = this.formBuilder.group({
      'nama_pelajar' : [null, Validators.required],
      'title' : [null, Validators.required],
      'Gender' : [null, Validators.required],
      'no_matrik' : [null, Validators.required],
      'faculty' : [null, Validators.required],
      'school' : [null, Validators.required],
      'nationality' : [null, Validators.required],
      'races' : [null, Validators.required],
      'address' : [null, Validators.required],
      'email' : [null, Validators.required],
      'phone' : [null, Validators.required]
    });
  }

  onFormSubmit(form:NgForm) {
    this.api.postBook(form)
      .subscribe(res => {
          let id = res['_id'];
          this.router.navigate(['/book-details', id]);
        }, (err) => {
          console.log(err);
        });
  }
}
